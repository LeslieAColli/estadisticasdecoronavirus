package estadisticasdecoronavirus;
import java.util.Scanner;

/**
 * Proyecto que muestra las estadísticas del coronavirus de la semana del 1 al 7 de junio
 * @author      Jessica Johana Estefanìa Chin Pech
 * @author      Leslie Alejandra Colli Pech
 * @author      Karen Estefanía Pérez Pérez
 * @author      Omar Jasiel Rodriguez Cab
 * @author      Diana Carolina Uc Vázquez
 * @author      Universidad Autónoma de Campeche
 * @since       Junio 2020
 * @version     2.0
 */

public class EstadisticasDeCoronavirus {

    /**
     * Método que imprime el mensaje y salta una línea
     * @param sMsj 
     */
    static void imprimirMsjL(String sMsj){
        System.out.println(sMsj);
    }
    
    /**
     * Método que imprime un separador
     */
    static void separador(){
        imprimirMsjL("-----------------------------------------------------------------------------");
    }
    
    /**
     * Método que imprime un separador con doble línea
     */
    static void sepdoble(){
        imprimirMsjL("============================================================================");
    }
    
    /**
     * Método que imprime información del programa y de los integrantes del equipo
     */
    static void infoP(){
        imprimirMsjL("                      Universidad Autónoma de Campeche");
        imprimirMsjL("                           Facultad de Ingeniería");
        imprimirMsjL("                  Ingeniería en Sistemas Computacionales");
        imprimirMsjL("                      Lenguajes de Programación I, 2A");        
        imprimirMsjL("                  Proyecto 7: Estadísticas de Coronavirus"+ "\n");
        imprimirMsjL("Integrantes del equipo:");
        imprimirMsjL("57039.- Chin Pech Jessica J. Estefanía.");
        imprimirMsjL("56618.- Colli Pech Leslie A.");
        imprimirMsjL("57569.- Pérez Pérez Karen E.");
        imprimirMsjL("56964.- Rodriguez Cab Omar J.");
        imprimirMsjL("57618.- Uc Vazquez D. Carolina.");
        separador();
    }    
    
    /**
     * Método que imprime información final (pie de página)
     */
    static void piePag(){
    imprimirMsjL("=UAC-FDI-ISC-LDP1-2A-ESTADÍSTICAS-DE-CORONAVIRUS=");
    }
    
    /**
     * Método que contiene el menú con opciones posibles a realizar
     */
    static void menu(){
      
        imprimirMsjL("MENÚ PRINCIPAL");
        imprimirMsjL("1.- Ingresar.");
        imprimirMsjL("2.- Salir.");
        imprimirMsjL("Ingrese el número de opción elegido:");
        separador();
       
    }
    
    /**
     * Realiza la opción del menú deseada, muestra las opciones del submenú y las ejecuta
     * @param iOpcion es una variable de tipo entero que sirve para ejecutar la alternativa de su preferencia en el menú y el submenú
     */
    static void opMenu(int iOpcion){
      
        switch (iOpcion){
            
            case 1:
                
                while (iOpcion != 8) {
                   
                    Scanner tec = new Scanner (System.in);
                    separador();
                    imprimirMsjL("ESTADÍSTICAS DE LA SEMANA.");
                    imprimirMsjL("1.- Visualizar los datos del lunes 1 de junio");
                    imprimirMsjL("2.- Visualizar los datos del martes 2 de junio");
                    imprimirMsjL("3.- Visualizar los datos del miércoles 3 de junio");
                    imprimirMsjL("4.- Visualizar los datos del jueves 4 de junio");
                    imprimirMsjL("5.- Visualizar los datos del viernes 5 de junio");
                    imprimirMsjL("6.- Visualizar los datos del sábado 6 de junio");
                    imprimirMsjL("7.- Visualizar los datos del domingo 7 de junio");
                    imprimirMsjL("8.- Regresar al menú principal");
                    imprimirMsjL("Ingrese el número de opción elegido:");
                    separador();
                    iOpcion = tec.nextInt();
                    
                        if (iOpcion == 1){
                            separador();
                            System.out.print(String.format("%-20s", "Dato"));
                            System.out.print(String.format("%-15s", "Cantidad"));
                            System.out.println(String.format("%-10s", "Gráfica"));
                            
                            Datos dato1= new Datos("Casos confirmados", 709, "=======");  
                            System.out.print(dato1.toString());
                            Datos dato2= new Datos("Casos activos", 164, "=------");  
                            System.out.print(dato2.toString());
                            Datos dato3= new Datos("Casos recuperados", 454, "====-----");  
                            System.out.print(dato3.toString());
                            Datos dato4= new Datos("Defunciones", 74, "-------");  
                            System.out.print(dato4.toString()+ "\n");
                            imprimirMsjL("Las líneas dobles representan las centenas y las líneas simples las decenas"+"\n");
                            sepdoble();
                        }
                            
                        if (iOpcion == 2){
                            separador();
                            System.out.print(String.format("%-20s", "Dato"));
                            System.out.print(String.format("%-15s", "Cantidad"));
                            System.out.println(String.format("%-10s", "Gráfica"));
                            
                            Datos dato5= new Datos("Casos confirmados", 712, "=======-");  
                            System.out.print(dato5.toString());
                            Datos dato6= new Datos("Casos activos", 148, "=----");  
                            System.out.print(dato6.toString());
                            Datos dato7= new Datos("Casos recuperados", 472, "====-------");  
                            System.out.print(dato7.toString());
                            Datos dato8= new Datos("Defunciones", 74, "-------");  
                            System.out.print(dato8.toString()+ "\n");
                            imprimirMsjL("Las líneas dobles representan las centenas y las líneas simples las decenas"+"\n");
                            sepdoble();
                        }
                            
                        if (iOpcion == 3){
                            separador();
                            System.out.print(String.format("%-20s", "Dato"));
                            System.out.print(String.format("%-15s", "Cantidad"));
                            System.out.println(String.format("%-10s", "Gráfica"));
                            
                            Datos dato9= new Datos("Casos confirmados", 736, "=======---");  
                            System.out.print(dato9.toString());
                            Datos dato10= new Datos("Casos activos", 119, "=-");  
                            System.out.print(dato10.toString());
                            Datos dato11= new Datos("Casos recuperados", 494, "====---------");  
                            System.out.print(dato11.toString());
                            Datos dato12= new Datos("Defunciones", 75, "-------");  
                            System.out.print(dato12.toString()+ "\n");
                            imprimirMsjL("Las líneas dobles representan las centenas y las líneas simples las decenas"+"\n");
                            sepdoble();
                        }
                       
                        if (iOpcion == 4){
                            separador();
                            
                            Datos dato13= new Datos("Casos confirmados", 780, "=======--------");  
                            System.out.print(dato13.toString());
                            Datos dato14= new Datos("Casos activos", 139, "=---");  
                            System.out.print(dato14.toString());
                            Datos dato15= new Datos("Casos recuperados", 507, "=====");  
                            System.out.print(dato15.toString());
                            Datos dato16= new Datos("Defunciones", 84, "--------");  
                            System.out.print(dato16.toString()+ "\n");
                            imprimirMsjL("Las líneas dobles representan las centenas y las líneas simples las decenas"+"\n");
                            sepdoble();
                        }
                        
                        if (iOpcion == 5){
                            separador();
                            System.out.print(String.format("%-20s", "Dato"));
                            System.out.print(String.format("%-15s", "Cantidad"));
                            System.out.println(String.format("%-10s", "Gráfica"));
                            
                            Datos dato17= new Datos("Casos confirmados", 788, "=======--------");  
                            System.out.print(dato17.toString());
                            Datos dato18= new Datos("Casos activos", 121, "=--");  
                            System.out.print(dato18.toString());
                            Datos dato19= new Datos("Casos recuperados", 543, "=====----");  
                            System.out.print(dato19.toString());
                            Datos dato20= new Datos("Defunciones", 84, "--------");  
                            System.out.print(dato20.toString()+ "\n");
                            imprimirMsjL("Las líneas dobles representan las centenas y las líneas simples las decenas"+"\n");
                            sepdoble();
                        }
                        
                        if (iOpcion == 6){
                            separador();
                            System.out.print(String.format("%-20s", "Dato"));
                            System.out.print(String.format("%-15s", "Cantidad"));
                            System.out.println(String.format("%-10s", "Gráfica"));
                            
                            Datos dato21= new Datos("Casos confirmados", 826, "========--");  
                            System.out.print(dato21.toString());
                            Datos dato22= new Datos("Casos activos", 128, "=--");  
                            System.out.print(dato22.toString());
                            Datos dato23= new Datos("Casos recuperados", 572, "=====-------");  
                            System.out.print(dato23.toString());
                            Datos dato24= new Datos("Defunciones", 86, "--------");  
                            System.out.print(dato24.toString()+ "\n");
                            imprimirMsjL("Las líneas dobles representan las centenas y las líneas simples las decenas"+"\n");
                            sepdoble();
                        }
                        
                        if (iOpcion == 7){
                            separador();
                            System.out.print(String.format("%-20s", "Dato"));
                            System.out.print(String.format("%-15s", "Cantidad"));
                            System.out.println(String.format("%-10s", "Gráfica"));
                            
                            Datos dato25= new Datos("Casos confirmados", 856, "========-----");  
                            System.out.print(dato25.toString());
                            Datos dato26= new Datos("Casos activos", 134, "=---");  
                            System.out.print(dato26.toString());
                            Datos dato27= new Datos("Casos recuperados", 592, "=====---------");  
                            System.out.print(dato27.toString());
                            Datos dato28= new Datos("Defunciones", 86, "--------");  
                            System.out.print(dato28.toString()+ "\n");
                            imprimirMsjL("Las líneas dobles representan las centenas y las líneas simples las decenas"+"\n");
                            sepdoble();
                        }
                        
                         if (iOpcion>8||iOpcion<1){
                            separador(); 
                            imprimirMsjL("Error de opción, intente de nuevo con un número válido");
                            sepdoble();
                        }
        }
                        
           break;
            
            case 2:
                separador();
                imprimirMsjL("¡Vuelva pronto!");
                separador();
                break;
            default: 
                separador();
                imprimirMsjL("Error de opción");
                separador();
        }
    }
    
    /**
     * Método que permite ejecutar el programa
     * @param args 
     */
    public static void main(String[] args) {
        separador();
        infoP(); 
          int iOpcion;
       do {
         menu();
         Scanner entrada = new Scanner(System.in);
         iOpcion = entrada.nextInt(); 
         opMenu(iOpcion);
       } while (iOpcion!=2);
       piePag();
       sepdoble();
    }
}
