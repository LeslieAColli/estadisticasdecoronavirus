package estadisticasdecoronavirus;

/**
 * Clase para datos
 * @author      Jessica Johana Estefanìa Chin Pech
 * @author      Leslie Alejandra Colli Pech
 * @author      Karen Estefanía Pérez Pérez
 * @author      Omar Jasiel Rodriguez Cab
 * @author      Diana Carolina Uc Vázquez
 * @author      Universidad Autónoma de Campeche
 * @since       Junio 2020
 * @version     2.0
 */
public class Datos {
    
    /**
     * Atributos (dato, cantidad, gráfica)
     */
    private String dato;
    
    private int cantidad;

    private String grafica;

    /**
     * Método constructor que incicializa el objeto Datos que contiene la información
     */
    public Datos() {
    }

    public Datos(String dato, int cantidad, String grafica) {
        this.dato = dato;
        this.cantidad = cantidad;
        this.grafica = grafica;
    }
    
    
    /**
     * Método get que muestra el nombre del dato
     * @return nombre del dato
     */
    public String getDato() {
        return dato;
    }
    
    /**
     * Método set que modifica el nombre del dato
     * @param dato 
     */
    public void setDato(String dato) {
        this.dato = dato;
    }

    /**
     * Método get que muestra la cantidad de casos
     * @return cantidad
     */
    public int getCantidad() {
        return cantidad;
    }

    /**
     * Método set que modifica la cantidad de casos
     * @param cantidad 
     */
    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }

    /**
     * Método get que muestra las gráficas de los datos
     * @return grafica
     */
    public String getGrafica() {
        return grafica;
    }

    /**
     * Método set que modifica la gráfica
     * @param grafica 
     */
    public void setGrafica(String grafica) {
        this.grafica = grafica;
    }

    /**
     * Método toString que imprime los datos estadísticos
     * @return dato, cantidad y grafica
     */
    @Override
    public String toString() {
        return String.format("%-20s", dato)+ String.format("%-15s", cantidad)+ String.format("%-10s", grafica)+ "\n";
        
    }

}